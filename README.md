# About
This project provides a way to display a PDF export of a PowerPoint presentation from Microsoft PowerPoint or a Keynote from Apple iWork's Keynote directly in the browser using only the javascript library [`PDF.js`](https://github.com/mozilla/pdf.js) from Mozilla. 

The benefit of being able to do this means you can host the Keynote or PowerPoint presentation on a static website and still have the user be able to flip through slides in a friendly interface, `Cmd + F` to find content in the slides, etc. without downloading the PDF to their computer. You can also go full screen and present the presentation just like Google Drive Slides all from the browser without downloading the PDF and opening it in Adobe Acrobat Reader or Preview.

# Features
- Sleek full black interface for viewing PDF PowerPoint and Keynote presentations
- Full screen presentation mode
- Arrow key support for switching slides
- The user is still able to download a PDF copy of the presentation from the interface if they wish.
- Jump to a specific slide/page
- Print mode
- Get a URL to a specific slide / scroll position
- 'Find' feature that finds text matches to a search query and lets you jump to them
- The interface is fully internationalized as it is forked from a PDF viewer built by Mozilla.
- The presentation title (from the PDF metadata) is shown at the top of the page and the title of browser window / tab is changed as well
- Supports viewing the presentation on mobile devices (and suggests to the user to turn the device to landscape orientation)
- Project is fully self-contained and requires no server-side rendering

# Usage
1. Simply replace `keynote.pdf` (located at `/keynote/keynote.pdf`) with your presentation.
2. Run `python -m SimpleHTTPServer 8000`.
3. Navigate to `localhost:8000` in your browser.

# Interface
![KeynoteViewer Interface](https://gitlab.com/ajayp/KeynoteViewer/raw/master/interface/interface.png)